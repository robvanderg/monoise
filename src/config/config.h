#ifndef INCLUDED_CONFIG_
#define INCLUDED_CONFIG_

#include <vector>
#include <string>

struct Config
{
    // paths for feature generation
    std::string w2v;
    std::string twitter;
    std::string wiki;
    std::string dict;

    std::string knowns;
    std::string aspLan;
    std::string aspDict;
    std::string aspMode;
    std::string dev;

    // binary args
    bool normAll;
    bool caps;
    bool goldErrDet;
    bool goldErrDet2;
    bool tokenize;
    bool wordPerLine;
    bool verbose;
    bool train;
    size_t onlyCanon;
    bool treebankMode;
    bool sepErrDet;
    double errDetWeight;

    // misc config options
    size_t numTrain;
    size_t numThreads;
    size_t numCands;
    double weight;
    size_t seed;
    size_t numTrees;
    std::string header;
    std::string goldTreebank;
    size_t kfold;

    // classifier options
    std::string cleanedPath;
    std::string regrPath;
    std::string featsPath;
    std::string regrDetPath;
    std::string featsDetPath;
    std::string lookupPath;
    size_t numFeats;
    std::vector<bool> featGroups;
    std::vector<bool> singleFeats;

    // used as features in Model::gen()
    std::vector<double> idxs;

    public:
        Config(){};
    private:
};

#endif
