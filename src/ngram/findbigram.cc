#include "ngram.ih"

uint32_t NGram::findBigram(uint64_t id, uint32_t beg, uint32_t end)
{
    uint32_t split = (beg + end) / 2;
    
    if (beg == end || beg == split)
        return 0;    
    if (d_bigrams[split] < id)
        return findBigram(id, split, end);
    if (d_bigrams[split] > id)
        return findBigram(id, beg, split);
    return split;
}

