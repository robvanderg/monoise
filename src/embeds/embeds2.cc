#include "embeds.ih"

Embeds::Embeds(string const &vec, string const &cache, bool bin)
:
    d_rawW2V(vec)
{
    if (bin)
        loadBin(cache);
    else
        loadTxt(cache);
}
