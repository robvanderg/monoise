#include "vocab.ih"

void Vocab::saveBin(ofstream *ofs)
{
    uint64_t size;
    size = d_vocab.size();
    ofs->write(reinterpret_cast<char*>(&size), sizeof(uint64_t));
    ofs->write(&d_vocab[0], sizeof(char) * size);

    size = d_idxs.size();
    ofs->write(reinterpret_cast<char*>(&size), sizeof(uint64_t));
    ofs->write(reinterpret_cast<char*>(&d_idxs[0]), sizeof(uint32_t) * d_idxs.size());
}
