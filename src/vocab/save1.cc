#include "vocab.ih"

void Vocab::save(string const &path)
{
    ofstream ofs(path);
    if (!ofs.good())
    {
        cerr << "Could not write vocab: " << path << '\n';
        exit(1);
    }
    save(&ofs);
}
