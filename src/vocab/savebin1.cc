#include "vocab.ih"

void Vocab::saveBin(string const &path)
{
    ofstream ofs(path, ios::binary);
    if (!ofs.good())
    {
        cerr << "Could not write vocab: " << path << '\n';
        exit(1);
    }
    saveBin(&ofs);
    ofs.close();
}
