#include "model.ih"

bool Model::consider()
{
    // Use gold error detection
    if (d_config->goldErrDet)
        return d_origs[d_wordIdx] != d_cors[d_wordIdx];

    if (d_config->goldErrDet2)
        return d_oov[d_wordIdx];
    
    // Lookup if this is a known word
    if (!d_config->normAll)
        return !d_knowns.contains(d_origs[d_wordIdx]);

    // don't normalize comments:
    if (d_config->wordPerLine && d_origs[d_wordIdx][0] == '#')
        return false;

    // Do not normalize words which are standardized in treebanks
    if (d_config->treebankMode)
    {
        string orig = toLower(d_origs[d_wordIdx]);
        if (orig == "ca" || orig == "'m" || orig == "'re" || orig == "n't" 
            || orig == "'ve" || orig == "'ll")
            return false;
    }
    return true;
}

