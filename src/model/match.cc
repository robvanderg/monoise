#include "model.ih"

bool Model::match(string const &orig, string const &cand)
{
    size_t foundIdx = 0;
    char prevC = ' ';
    for( char c: orig)
    {
        bool found = false;
        if (c == prevC)
            found = true;
        else    
            for (size_t beg = foundIdx; beg != cand.size(); ++ beg)
            {
                if (c == cand[beg])
                {
                    foundIdx = beg;
                    found = true;
                }
            }

        if (found == false)
            return false;
        prevC = c;
    }
    return true;

}
