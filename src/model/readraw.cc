#include "model.ih"

bool Model::readRaw(istream *input)
{
    d_origs.clear();
    string line = "-";
    if (d_config->wordPerLine)
    {
        while (true)
        {
            if(!getline((*input), line))
                return !d_origs.empty();
            if (line == "" || line == " ")
                return true;
            if (!d_config->caps)
                line = toLower(line);
            vector<string> splitted;
            splitString(line, '\t', splitted);
            d_origs.push_back(splitted[0]);
        }
    }

    else if (getline((*input), line))
    {
        if (!d_config->caps)
            line = toLower(line);
        string word;
        stringstream lineStream(line);
        while (lineStream >> word)
        {
            if (d_config->tokenize)
                for (string token: tokenize(word, ".\"?!&*(){}:;/,~\\"))
                    d_origs.push_back(token);
            else
                d_origs.push_back(word);
        }
        return true;
    }
    return false;
}
