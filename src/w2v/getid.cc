#include "w2v.ih"

int w2v::getId(string const &word)
{
    int ret = 0;
    int id = d_myVocab.getId(word);
    if (id != 0)
        ret = d_vocabLink[id];
    return ret;
}
