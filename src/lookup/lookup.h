#ifndef INCLUDED_LOOKUP_
#define INCLUDED_LOOKUP_

#include <map>
#include <string>
#include <stdint.h>
#include "../vocab/vocab.h"

class Lookup
{
    std::map<std::string, std::map<std::string, double>> d_data;
    Vocab d_knowns;

    public:
        Lookup(std::string const &path = "");

        void addWord(std::string const &src, std::string const &tgt);
        void save(std::string const &path);
        void load(std::string const &path);

        std::map<std::string, double> getLookups(std::string const &word);
        bool contains(std::string const &word);
        bool isCanon(std::string const &word);

        void optimize(){d_knowns.optimize();};
    private:
};
        
#endif
