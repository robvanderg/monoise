#include "config/main.ih"


int main(int argc, char *argv[])
{
    argc-=(argc>0); argv+=(argc>0); // skip program name argv[0] if present
    option::Stats  stats(usage, argc, argv);
    std::vector<option::Option> options(stats.options_max);
    std::vector<option::Option> buffer(stats.buffer_max);
    option::Parser parse(usage, argc, argv, &options[0], &buffer[0]);

    if (parse.error() || options[HELP] || argc == 0)
    {
        option::printUsage(std::cerr, usage);
        return (parse.error())? 1: 0;
    }

    if (!options[MODE])
    {
        std::cerr << "Please use -m to specify mode: TRain TEst or RUn\nOr run ./monoise -h for help.\n";
        return 0;
    }
    
    enum Mode { TRAIN, TEST, RUN, INT, KF, DEMO, UNK};
    Mode curMode = UNK; 

    char mode1 = toupper(options[MODE].arg[0]);
    char mode2 = toupper(options[MODE].arg[1]);
    if (mode1 == 'T' && mode2 == 'R')
        curMode = TRAIN; 
    else if (mode1 == 'T' && mode2 == 'E')
        curMode = TEST;
    else if (mode1 == 'R' && mode2 == 'U')
        curMode = RUN;
    else if (mode1 == 'I' && mode2 == 'N')
        curMode = INT;
    else if (mode1 == 'K' && mode2 == 'F')
        curMode = KF;
    else if (mode1 == 'D' && mode2 == 'E')
    {
        server();
        return 0;
    }
    if (!options[RANDOMF])
    {
        std::cerr << "Please specify path to random forest classifier with -r\n";
        return 0;
    }

    if (!options[DIR])
    {
        std::cerr << "Please specify path to directory with data -d\n";
        return 0;
    }

    Config myConfig;
    myConfig.train = (curMode == TRAIN || curMode == KF);
    readConfig(&myConfig, options);
    Model myModel(&myConfig);
 
    std::istream *input = &std::cin; 
    std::ifstream in2;//Cant this be done simpler?
    if(options[INPUT])
    {
        in2.open(options[INPUT].arg);
        input = &in2;
    }
    if (!input->good())
    {
        std::cerr << "Could not read input\n";
        exit(1);
    }
    std::ostream *output = &std::cout;
    std::ofstream out2;
    if(options[OUTPUT])
    {
        out2.open(options[OUTPUT].arg);
        output = &out2;
    } 
    if (!output->good())
    {
        std::cerr << "Could not write output\n";
        exit(1);
    }
    std::ostream *output2 = &std::cerr;
    switch(curMode)
    {
        case TRAIN:
            if (!myConfig.sepErrDet)
                myModel.train(options[INPUT].arg, output);
            else
                myModel.train2(options[INPUT].arg, output);
            if (myConfig.dev != "")
            {
                std::ifstream in(myConfig.dev);
                myModel.test(&in, output);
            }
            break;

        case TEST:
            myModel.test(input, output);
            break;

        case RUN:
            myModel.run(input, output);
            break;

        case INT:
            myModel.interactive();
            break;

        case KF:
            myModel.kFold(options[INPUT].arg, output, output2);
            break;

        case UNK:
            std::cerr << "Unknown mode entered as argument; please use TRain TEst or RUn\nOr try ./monoise -h for help\n";
            break;
        case DEMO:
            break;
    }
    in2.close();
    out2.close();
}
