#include "server.ih"

string Server::normalize(string const &txt, int langIdx, double weight)
{
    return d_models[langIdx]->run2(txt, weight);
}
