#include "server.ih"

bool Server::checkIp(char *ipstr)
{// based on: https://stackoverflow.com/questions/3060950/how-to-get-ip-address-from-sock-structure-in-c
    if (cli_addr.ss_family == AF_INET) { //IPv4
        struct sockaddr_in* pV4Addr = (struct sockaddr_in*)&cli_addr;
        struct in_addr ipAddr = pV4Addr->sin_addr;
        char str[INET_ADDRSTRLEN];
        inet_ntop( AF_INET, &ipAddr, str, INET_ADDRSTRLEN );
        return (strcmp(str, "130.226.142.28") == 0);
    } else { // IPv6
        struct sockaddr_in6* pV6Addr = (struct sockaddr_in6*)&cli_addr;
        struct in6_addr ipAddr       = pV6Addr->sin6_addr;
        char str[INET6_ADDRSTRLEN];
        inet_ntop( AF_INET6, &ipAddr, str, INET6_ADDRSTRLEN );
        return (strcmp(ipstr, "130.226.142.28") == 0);
    }
}
