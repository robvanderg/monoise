for cap in [True, False]:
    for bad in [True, False]:
        for test in [True, False]:
            name = 'it' + ('.cap' if cap else '') + ('.bad' if bad else '') + ('.test' if test else '')
            workingFile = '../working/' + name;
            predFile = '../preds/' + name;
            cmd = './tmp/bin/binary -m KF -i ../data/it/traindev -d ../data/it/'
            cmd += ' -r ' + workingFile
            if cap:
                cmd += ' -C'
            if bad:
                cmd += ' -b'
            if test:
                cmd = cmd.replace('KF', 'TR')
                cmd += ' -D ../data/it/test'
            print(cmd + ' > ' + predFile + '.out' + ' 2> ' + predFile + '.err')
            if not test:
                cmd += ' -f 111101111110'
                predFile = predFile.replace(name, name + '.old')
                cmd = cmd.replace(name, name + '.old')
                print(cmd + ' > ' + predFile + '.out' + ' 2> ' + predFile + '.err')

