def getAccERR2(name):
    accuracy = 0
    ERR = 0
    for line in open(name):
        if line.startswith('    1') and accuracy == 0:
            accuracy = float(line.strip().split()[-1])
        if line.startswith('ERR') and ERR == 0:
            ERR = float(line.strip().split()[-1])
    return accuracy * 100, ERR * 100

def getAccERR(name):
    accuracy = []
    ERR = []
    for line in open(name):
        if line.startswith('    1'):
            accuracy.append(float(line.strip().split()[-1]))
        if line.startswith('ERR'):
            ERR.append(float(line.strip().split()[-1]))
    accSum = 0.0
    for acc in accuracy:
        accSum += acc * 100
    errSum = 0.0
    for err in ERR:
        errSum += err * 100
    return accSum / len(accuracy), errSum / len(ERR)

def getBase(name):
    accuracy = 0
    for line in open(name):
        if line.startswith('Baseline') and accuracy == 0:
            accuracy = float(line.strip().split()[-2])
    return accuracy * 100, 0.0

def getBase2(name):
    accuracy = []
    for line in open(name):
        if line.startswith('Baseline') and accuracy == 0:
            accuracy.append(float(line.strip().split()[-2]))
    accSum = 0.0
    for acc in accuracy:
        accSum += acc * 100
    return accSum / len(accuracy), 0.0

#data = [getBase('it.out'), getAccERR('it.out'), getBase('it.cap.old.out'), getAccERR('it.cap.old.out'), getAccERR('it.cap.out')]
data = [getBase('../preds/it.bad.out'), getAccERR('../preds/it.bad.out'), getBase('../preds/it.cap.bad.old.out'), getAccERR('../preds/it.cap.bad.old.out'), getAccERR('../preds/it.cap.bad.out')]

print('\\begin{tabular}{l r r | r r r}')
print('\\toprule')
print ( '\\multicolumn{2}{c}{lowercased} & \\multicolumn{3}{c}{} \\\\ ' )
print(' & '.join(['','Base', 'MoNoise', 'Base', 'MoNoise', '+capFeats']) + '\\\\')

print('\\midrule')
print('Acc.', end='')
for item in data:
    print(' & ' + '{:.2f}'.format(item[0]), end = '')
print('\\\\')

print('ERR', end='')
for item in data:
    print(' & ' + '{:.2f}'.format(item[1]), end = '')
print('\\\\')

print('\\bottomrule')
print('\\end{tabular}')

