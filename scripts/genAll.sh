grep Upperbound preds/gen*out > results
python3 scripts/1.gen.graph.py results
python3 scripts/1.gen.table.py > gen.tex

grep "^ERR" preds/*rank*out > results
grep "^Normalization" preds/*rank*out > results2
python3 scripts/2.rank.graph.py results results2

grep "^ERR" preds/learning* > results
python3 scripts/3.learningC.graph.py results 

grep "^ *[0-9]" preds/rank.*11111111111* > results
grep "^Upperbound" preds/rank.*11111111111* > results2
python3 scripts/4.topN.graph.py results results2

#grep "^ERR" preds/tuneW.*out > results
#python3 scripts/6.tuneW.graph.py results 

grep "^ERR" preds/errDet.* > results
python3 scripts/7.errDet.graph.py results 
python3 scripts/7.errDet.table.py > errDet.tex

python3 scripts/8.test.table.py > test.tex

rm results
rm results2
