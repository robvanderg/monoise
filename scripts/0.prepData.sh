mkdir data
cd data
for lang in en es hr nl sl sr it tr;
do
    if [ ! -d $lang ]; then
        echo http://www.itu.dk/people/robv/data/monoise/"$lang".tar.gz 
        curl http://www.itu.dk/people/robv/data/monoise/"$lang".tar.gz | tar xvz
    fi
done


