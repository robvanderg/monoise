# Make sure config.en uses all train data!
# And config.nl numTrain 770/192 instead of 578

cd src

./tmp/bin/binary -n 12 -b -m TR -i ../data/enData/chenli -r ../working/chenli -g -f 1111011111 
./tmp/bin/binary -n 12 -b -m TE -i ../data/enData/lexnorm1.2 -r ../working/chenli -g -f 1111011111  
./tmp/bin/binary -n 12 -b -m TE -i ../data/enData/lexnorm_chenli -r ../working/chenli -g -f 1111011111  

./tmp/bin/binary -n 12 -b -m TR -i ../data/enData/lexnorm2015.train -r ../working/lexnorm2015 
./tmp/bin/binary -n 12 -b -m TE -i ../data/enData/lexnorm2015.test -r ../working/lexnorm2015

./tmp/bin/binary -n 12 -b -m TR -i ../data/enData/chenli -r ../working/chenli -f 1111011111  
./tmp/bin/binary -n 12 -b -m TE -i ../data/enData/lexnorm1.2 -r ../working/chenli -f 1111011111  

./tmp/bin/binary -n 12 -b -m TR -i ../data/nlData/orphee -r ../working/orphee -l nl

