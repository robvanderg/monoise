cd src

for i in 1000000000 0100000000 0010000000 0001000000 0000100000 1111110000 0111110000 1011110000 1101110000 1110110000 1111010000 1111100000
do
    ./tmp/bin/binary -m TR -i ../data/enData/chenli -r ../working/chenliGold."$i" -g -f "$i" > ../runs/chenli.gen.$i &
    ./tmp/bin/binary -l nl -m TR -i ../data/nlData/orphee -r ../working/orpheeGold."$i" -g -f "$i" > ../runs/orphee.gen.$i &
    ./tmp/bin/binary -m TR -i ../data/enData/lexnorm2015.train -r ../working/lexnorm2015."$i" -g -f "$i" > ../runs/lexnorm2015.gen.$i 
done

grep Upperbound runs/*gen* > runs/genGraph.txt
python3 ../scripts/clin/gen.py ../runs/genGraph.txt

