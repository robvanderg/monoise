cd src

for i in 1111111111 0111111111 1011111111 1101111111 1110111111 1111011111 1111101111 1111110111 1111111011 1111111101 1111111110 1111111111
do
    ./tmp/bin/binary -m TR -i ../data/enData/chenli -r ../working/chenliGold."$i" -g -f "$i" > ../runs/chenli.feats.$i &
    ./tmp/bin/binary -m TR -i ../data/enData/lexnorm2015.train -r ../working/lexnorm2015."$i" -f "$i" -n 12 > ../runs/lexnorm2015.feats.$i
done

grep "^Normalization:" ../runs/lexnorm2015.feats.* > ../runs/feats.lexnorm2015
grep "^    1" ../runs/chenli.feats.* > ../runs/feats.chenli
python3.5 ../scripts/clin/feats.py ../runs/feats.chenli ../runs/feats.lexnorm2015

