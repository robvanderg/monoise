import myutils

for lang in myutils.langs:
    print("./tmp/bin/binary -m TR -i ../data/" + lang + "/traindev -r ../working/testModel." + lang + " -d ../data/" + lang + " -D ../data/" + lang + "/test > ../preds/test." + lang + '.out 2> ../preds/test.' + lang + '.err')
    print("./tmp/bin/binary -m TR -b -i ../data/" + lang + "/traindev -r ../working/testModel.bad." + lang + " -d ../data/" + lang + " -D ../data/" + lang + "/test > ../preds/test.bad." + lang + '.out 2> ../preds/test.bad.' + lang + '.err')
    print("./tmp/bin/binary -m TR -i ../data/" + lang + "/traindev -r ../working/testModel.g." + lang + " -d ../data/" + lang + " -D ../data/" + lang + "/test -g > ../preds/test." + lang + '.gold.out 2> ../preds/test.' + lang + '.gold.err')
    if lang == 'es':
        print("./tmp/bin/binary -m TR -i ../data/" + lang + "/traindev -r ../working/testModel.G." + lang + " -d ../data/" + lang + " -D ../data/" + lang + "/test -G > ../preds/test." + lang + '.Gold.out 2> ../preds/test.' + lang + '.gold.err')

print("./tmp/bin/binary -m TR -i ../data/sl/tweet.L1.traindev -r ../working/sl.l1 -d ../data/sl -D ../data/sl/tweet.L1.test > ../preds/test.sl.l1.out 2> ../preds/test.sl.l1.err")
print("./tmp/bin/binary -m TR -i ../data/sl/tweet.L3.traindev -r ../working/sl.l3 -d ../data/sl -D ../data/sl/tweet.L3.test > ../preds/test.sl.l3.out 2> ../preds/test.sl.l3.err")
print("./tmp/bin/binary -m TR -i ../data/tr/traindev -r ../working/tr -d ../data/tr -D ../data/tr/testSmall > ../preds/test.tr.small.out 2> ../preds/test.tr.small.err")

print("./tmp/bin/binary -b -m TR -i ../data/sl/tweet.L1.traindev -r ../working/sl.l1.bad -d ../data/sl -D ../data/sl/tweet.L1.test > ../preds/test.sl.l1.bad.out 2> ../preds/test.sl.l1.bad.err")
print("./tmp/bin/binary -b -m TR -i ../data/sl/tweet.L3.traindev -r ../working/sl.l3.bad -d ../data/sl -D ../data/sl/tweet.L3.test > ../preds/test.sl.l3.bad.out 2> ../preds/test.sl.l3.bad.err")
print("./tmp/bin/binary -b -m TR -i ../data/tr/traindev -r ../working/tr.bad -d ../data/tr -D ../data/tr/testSmall > ../preds/test.tr.small.bad.out 2> ../preds/test.tr.small.bad.err")
