import sys
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
import myutils

if len(sys.argv) < 3:
    print("usage:")
    print("grep \"^ERR\" preds/*rank*out > results")
    print("grep \"^Normalization\" preds/*rank*out > results2")
    print("python3 scripts/2.rank.graph.py results results2")
    exit(1)

data = {}
for line in open(sys.argv[1]):
    line = line[line.rfind('/')+1:]
    tok=line.split()
    lang = tok[0].split('.')[1]
    feats = tok[0].split('.')[2]
    err = float(tok[-1])

    if lang not in data:
        data[lang] = {'abl':[0] * 12, 'all':0}
    if feats.count('0') == 0: # all
        data[lang]['all'] = [err,0,0]
    else: # ablation 
        featIdx = feats.find('0')
        data[lang]['abl'][featIdx] = [err,0,0]

for line in open(sys.argv[2]):
    line = line[line.rfind('/')+1:]
    tok=line.split()
    lang = tok[0].split('.')[1]
    feats = tok[0].split('.')[2]
    rec = float(tok[-3])
    prec = float(tok[-2])

    if lang not in data:
        data[lang] = {'abl':[0] * 12, 'all':0}
    if feats.count('0') == 0: # all
        data[lang]['all'][1] = rec
        data[lang]['all'][2] = prec
    else: # ablation 
        featIdx = feats.find('0')
        data[lang]['abl'][featIdx][1] = rec
        data[lang]['abl'][featIdx][2] = prec


def graph(which, location, prefix, ySize, outName):
    fig, ax = plt.subplots(figsize=(8,5), dpi=300)

    prec = []
    rec = []
    err = []
    if which == 'langs':
        for langIdx, lang in enumerate(myutils.langs):
            print(lang, data[lang]['all'])
            err.append(data[lang]['all'][0])
            rec.append(data[lang]['all'][1])
            prec.append(data[lang]['all'][2])
    else:
        errAll = 0
        recAll = 0
        precAll = 0
        for lang in myutils.langs:
            errAll += data[lang]['all'][0]
            recAll += data[lang]['all'][1]
            precAll += data[lang]['all'][2]
        for featIdx, feat in enumerate(myutils.featNames):
            errTotal = 0.0
            recTotal = 0.0
            precTotal = 0.0
            for lang in myutils.langs:
                errTotal += data[lang]['abl'][featIdx][0]
                recTotal += data[lang]['abl'][featIdx][1]
                precTotal += data[lang]['abl'][featIdx][2]
            err.append((errAll - errTotal) / len(myutils.langs))
            rec.append((recAll - recTotal) / len(myutils.langs))
            prec.append((precAll - precTotal) / len(myutils.langs))
    
    bar_width = .25
    index1 = []
    index2 = []
    index3 = []
    for i in range(len(myutils.langs if which == 'langs' else myutils.featNames)):
        index1.append(i + bar_width )
        index2.append(i + bar_width * 2)
        index3.append(i + bar_width * 3)

    ax.bar(index1, err, bar_width, label='ERR', color=myutils.colors[0])
    ax.bar(index2, prec, bar_width, label='Precision', color=myutils.colors[1])
    ax.bar(index3, rec, bar_width, label='Recall', color=myutils.colors[2])

    if which == 'langs':
        myutils.setTicks(ax, myutils.corpora, 45)
        ax.set_xlabel('Corpus')
    else:
        myutils.setTicks(ax, myutils.featNames, 45)
        ax.set_xlabel('Feature Group')
    ax.set_ylabel('Degration in performance')
    ax.set_xlim((0,11))

    leg = ax.legend(loc=location)
    leg.get_frame().set_linewidth(1.5)
    fig.savefig(outName, bbox_inches='tight')
    #plt.show()

#graph('langs', 'lower right', '-', (0.2,1), 'precRecLang.pdf')
graph('feats', 'upper right', '-', (0.2,1), 'precRecFeat.pdf')

