import myutils

def getPerf(lang):
    err = prec = rec = 0
    for line in open('preds/test.' + lang + '.out'):
        if line.startswith("Normalization"):
            prec = float(line.split()[2])
            rec = float(line.split()[1])
        if line.startswith("ERR"):
            err = float(line.split()[1])
    return [err, prec, rec]

print('\\begin{table}')
print('    \\centering')
print('    \\begin{tabular} {l r r r}')
print('         \\toprule')
print('         Corpus        & ERR    & Precision & Recall \\\\')
print('         \\midrule')

for langIdx, lang in enumerate(myutils.langs):

    print('         ' + myutils.corpora[langIdx].ljust(13), end=' & ')
    print(' & '.join([str(round(x*100,2)).ljust(5,'0') for x in getPerf(lang)]) + ' \\\\')
    
print('         \\bottomrule')
print('    \\end{tabular}')
print('    \\caption{Results of MoNoise on the test data.}')
print('    \\label{tab:normTest}')
print('\\end{table}')


