./0.prep.sh


python3 scripts/1.monoise.dev.py > 1.dev.sh
chmod +x 1.dev.sh
./1.dev.sh

python3 scripts/1.monoise.dev.run.py > 1.dev.run.sh
chmod +x 1.dev.run.sh
./1.dev.run.sh

python3 scripts/2.monoise.test.py > 2.test.sh
chmod +x 2.test.sh
./2.test.sh

python3 scripts/2.monoise.test.run.py > 2.test.run.sh
chmod +x 2.test.run.sh
./2.test.run.sh

python3 scripts/2.monoise.test.fix.py

