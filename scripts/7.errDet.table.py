import myutils

def getPerf(path):
    for line in open(path):
        if line.startswith('ERR'):
            return  str(round(float(line.split()[-1])*100,2)).ljust(5,'0')
    return '0.00'

def getTime(path):
    words = 0.0
    time = 1.0
    for line in open(path):
        if line.startswith('Total'):
            words = float(line.split()[-1])
        if line.startswith('Testing time'):
            time = float(line.split()[-1])
    return str(int(round(float(words / time) ,0)))

print('\\begin{table}')
print('    \\centering')
print('    \\begin{tabular} {l | r r | r r}')
print('         \\toprule')
print('         Corpus        & \\multicolumn{2}{c}{MoNoise}  & \\multicolumn{2}{c}{+errDet} \\\\')
print('         \\midrule')
print('                      & ERR & words/ & ERR & words/ \\\\')
print('                      &     & second &     & second \\\\')
print('         \\midrule')
for langIdx, lang in enumerate(myutils.langs):
    
    base = getPerf('preds/errDet.' + lang + '.0.0.out') 
    timeBase = getTime('preds/errDet.' + lang + '.0.0.out')
    errDet = getPerf('preds/errDet.' + lang + '.voc.out')
    timeErrDet = getTime('preds/errDet.' + lang + '.voc.out')
    if float(timeErrDet) < 100:
        timeErrDet = '\\hspace{.2cm}' + timeErrDet
    corpus = myutils.corpora[langIdx]
    if corpus=="TweetNorm":
        corpus+="*"
    print('         ' + ' & '.join([corpus.ljust(13), base, timeBase, errDet, timeErrDet]) + ' \\\\')
print('         \\bottomrule')
print('    \\end{tabular}')
print('    \\caption{The effect of using a vocabulary to filter words to consider for normalization on the ERR (+errDet).  *the comparison is not completely fair on this dataset, since the annotation guidelines enforced that only out of vocabulary words are normalized.}')
print('    \\label{tab:errDet}')
print('\\end{table}')

