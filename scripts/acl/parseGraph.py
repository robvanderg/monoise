import matplotlib.pyplot as plt
import sys


def isfloat(value):
  try:
    float(value)
    return True
  except ValueError:
    return False

name = 0

data = []
data.append([0.0] * 10)
data.append([0.0] * 10)
counter = 0
for line in open(sys.argv[1]):
    if line == '\n':
        pass
    elif 'F1' in line:
        score = float(line.split()[-1])
        data[counter %3 -1][name] += score
    else:
        name = int(line.split()[1])
    counter += 1

for i in range(2):
    for j in range( len(data[0])):
        data[i][j] = data[i][j] / 10

names = ['UNK', 'ALL']
plt.plot(range(1, len(data[0])), data[0][1:], 'ro-', markersize=8, label=names[0])
print(names[0])
print(data[0], '\n')

plt.plot(range(1, len(data[1])), data[1][1:], 'b^-', markersize=8, label=names[1])
print(names[1])
print(data[1])

plt.plot(range(1, len(data[1])), [70.85] * (len(data[1])-1), 'gray', linestyle='--', label='VAN')

plt.legend(loc='lower right')
plt.xlabel(r'Number of normalization candidates used ($\alpha$)')
plt.ylabel('F1-score')
plt.ylim([70.5,72.75])
plt.savefig("parse.pdf")
plt.show()

