print(
"""
\\begin{table}
    \centering
  \\begin{tabular}{l | r r | r r}
    Cands($\\alpha$) & \multicolumn{2}{|c|}{1} & \multicolumn{2}{c}{6} \\\\
    \hline
    Weight($\\beta$) & UNK & ALL & UNK & ALL \\\\
    \cline{2-5}""")

data = {}
counter = 0
for line in open('util/table'):
    if line[0] == 'R':
        F1 = float(line.split()[-1])
        idx = counter % 5 - 1
        data[weight][idx] += F1
    else:
        weight = float(line.split()[1])
        if weight not in data:
            data[weight] = [0.0, 0.0, 0.0, 0.0]
    counter += 1

for weight in sorted(data):
    print ('   {0}  \t& {1:.2f}\t& {2:.2f}\t& {3:.2f}\t& {4:.2f} \\\\'.format(weight, data[weight][0]/10, data[weight][1]/10, data[weight][2]/10, data[weight][3]/10))

print(
"""  \end{tabular}
  \caption{F1 scores on the development data using different weights, comparing
            only using the best candidate versus using 6 candidates.}
  \label{tab:weights}
\end{table}
""")

