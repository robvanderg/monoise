mkdir working
wget http://www.let.rug.nl/rob/data/data.tar.gz
tar -zxvf data.tar.gz
rm data.tar.gz
mv enData/chenli.* working/

# train dev test data
wget http://www.hlt.utdallas.edu/~chenli/normalization/2577_tweets 
mv 2577_tweets enData/chenli
wget http://people.eng.unimelb.edu.au/tbaldwin/etc/lexnorm_v1.2.tgz
tar -zxvf lexnorm_v1.2.tgz
rm lexnorm_v1.2.tgz
mv data/corpus.v1.2.tweet enData/lexnorm
rm -r data

# Patch the lexnorm corpus, only ascii characters
perl -i.bak -pe 's/[^[:ascii:]]//g' enData/lexnorm 
grep -vP "^\tNO\t$" enData/lexnorm > enData/lexnorm2
mv enData/lexnorm2 enData/lexnorm
rm enData/lexnorm.bak


#TODO lexnorm2015
#tail -n 15360 enData/lexnorm2015.train > enData/lexnorm2015.dev
