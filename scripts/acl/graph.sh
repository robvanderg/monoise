# Note that training requires +- 20gb!
# Also note that this script is quite slow, it trains
# 10 models, and loads them all 10 times

for i in {0..9}; 
do 
    echo ./tmp/bin/binary -m TR -i ../data/en2/train -r ../working/chenli$i -C -d ../data/en/ -s $i
    #for CANDS in {1..9};
    #do
    #    echo $i $CANDS >> util/graph
    #    ./tmp/bin/binary -m TE -i ~/data/foster.tokens -r working/chenli$i -p ~/data/foster.ptb -c $CANDS -a -u >> util/graph
    #    ./tmp/bin/binary -m TE -i ~/data/foster.tokens -r working/chenli$i -p ~/data/foster.ptb -c $CANDS -a >> util/graph
    #done
done
#python3.5 util/genGraph.py
