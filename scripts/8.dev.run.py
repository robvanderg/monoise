

for lang in ['en1', 'en2', 'nl', 'es', 'sl', 'sr', 'hr']:
    print("./tmp/bin/binary -m TR -i ../data/" + lang + "/train -r ../working/model.old." + lang + " -d ../data/" + lang + " -D ../data/" + lang + "/dev > ../preds/dev.old." + lang + '.out 2> ../preds/dev.old.' + lang + '.err')
