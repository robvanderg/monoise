import sys
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
import myutils

if len(sys.argv) < 2:
    print("usage:")
    print("grep \"^ERR\" preds/learning* > results")
    print("python3 scripts/3.learningC.graph.py results ")
    exit(1)

data ={}
fig, ax = plt.subplots(figsize=(8,5), dpi=300)

for line in open(sys.argv[1]):
    tok=line.split()
    lang = tok[0].split('/')[1].split('.')[1]
    numTrain = int(tok[0].split('.')[2].split(':')[0])
    err = float(tok[-1])

    if lang not in data:
        data[lang] = {}
    data[lang][numTrain] = err


for langIdx, lang in enumerate(myutils.langs):
    x = []
    y = []
    for numTrain in sorted(data[lang]):
        x.append(numTrain)
        y.append(data[lang][numTrain])
    print(lang)
    print(x)
    print(y)
    style = '-'
    if langIdx > 3:
        style= '--'
    ax.plot(x, y, linestyle=style, label=myutils.corpora[langIdx], linewidth=3)
    
leg = ax.legend(loc='lower right', fontsize=12)
leg.get_frame().set_linewidth(1.5)

ax.set_ylabel('ERR')
ax.set_xlabel('Train size (words)')
plt.xticks(range(0,60000, 10000), ['0', '10,000', '20,000', '30,000', '40,000', '50,000', '60,000'])
ax.set_xlim((0,60000))
fig.savefig('learningC.pdf', bbox_inches='tight')
#plt.show()


