import myutils

for lang in myutils.langs + ['it']:
    bad = ''
    if lang in ['es', 'en1', 'en2', 'tr', 'it']:
        bad = ' -b '
    if lang in ['tr', 'it']:
        bad += ' -C '
    cmd = './tmp/bin/binary -m TR ' + bad + ' -d ../data/' + lang + ' -i ../data/' + lang + '/traindev -r ../data/' + lang + '/model  &> out.' + lang 
    print(cmd)
