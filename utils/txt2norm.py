import sys

if len(sys.argv) < 2:
    print('please give input file')
    exit(1)

punct = ".\"?!&*(){}:;/,~\\"

def tok(word):
    res = []

    # Split front
    for i in range(len(word)):
        if word[i] not in punct:
            break
    if i > 0:
        res.append(word[:i])
        word = word[i:]
    
    # split back
    for i in range(len(word)):
        i = len(word) - 1  - i 
        if word[i] not in punct:
            break
    if i < len(word) -1:
        res.append(word[:i+1])
        res.append(word[i+1:])
    else:
        res.append(word)
    return res

for line in open(sys.argv[1]):
    for word in line.split():
        for word in tok(word):
            print(word + '\tIV\t' + word)
    print()


