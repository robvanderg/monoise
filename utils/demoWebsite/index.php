<?php
    include 'normalize.php';
    if(isset($_POST['SubmitText']))
    { //check if form was submitted
        $input = $_POST['inputText'];
        $language = $_POST['selectLang'];
        $weight = $_POST['aggr'];
        $output = normalize($language, $weight, $input);
        $feedback = "y";
    }
    else
    {
        $output = "";
        $input = "";
        $weight= 70;
        $language= "en";
        $feedback= "n";
    }
?>


<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>MoNoise Demo</title>
        <link rel=stylesheet href="../css/main.css?v=<?=time();?>" type="text/css">
    </head>
    <body>
    <div id="content">
        <h2>MoNoise Demo</h2>
        <p>Welcome on the demo page of MoNoise; a modular normalization model. The demo on this page demonstrates a 'translation' of chat texts to more canonical language. The pretrained models are best suited for Twitter-like data, since they are trained on normalized Tweets. More information about how this demo works can be found in the <a href="http://www.let.rug.nl/rob/doc/clin27.pdf">paper</a> and the <a href="https://bitbucket.org/robvanderg/monoise">code</a>.</p>
        <p>To try the model, simply choose the language from the dropdown menu, type your text in the input box on the left and press submit. Note that you can tune the aggressiveness of the model using the slider on the left (which will only update after another submit).</p>
<hr>        

        <form action="#" method="post">
            <div id="slider">
                Conservative 
                <input type="range" min="0" max="100" value=<?php echo $weight; ?> class="slider" name="aggr"> 
                Aggressive
            </div>

            <div id="language">
                Language: 
                <select name="selectLang">
                    <option value="en" <?php if($language == "en"){echo "selected";} ?>>English</option>
                    <option value="nl" <?php if($language == "nl"){echo "selected";} ?>>Dutch</option>
                    <option value="hr" <?php if($language == "hr"){echo "selected";} ?>>Croatian</option>
                    <option value="sr" <?php if($language == "sr"){echo "selected";} ?>>Serbian</option>
                    <option value="sl" <?php if($language == "sl"){echo "selected";} ?>>Slovenian</option>
                    <option value="es" <?php if($language == "es"){echo "selected";} ?>>Spanish</option>
                    <option value="tr" <?php if($language == "tr"){echo "selected";} ?>>Turkish</option>
                </select>
            </div>

            
            <div id="button">    
                <input type="submit" name="SubmitText"/>
            </div>
            
            <div class="textBubble question">
                <div class="arrow-left"></div>
                <div class="text">
                    <textarea class="styled" maxlength="280" name="inputText" rows="4" placeholder="Enter your Tweet here:"><?php echo $input; ?></textarea>
                </div>
            </div>

            <div class="textBubble answer">
                <div class="arrow-right "></div>
                <div class="text">
                    <textarea class="styled" maxlength="2048" name="outputText" rows="4" placeholder="The normalization will appear here" readonly><?php echo trim($output); ?></textarea>
                </div>
            </div>
            </form>
    <hr style="margin-top:185px;">
    <p>This system is developed as part of the Parsing Algorithms for Uncertain Input project, more information can be found on the <a href="http://www.let.rug.nl/rob/pafui.htm">Home Page</a>. For any questions or bug reports, please contact Rob van der Goot (r.van.der.goot@rug.nl).</p>
  </div>
  <div id="tweets">
    <!--<h4>Example Tweets</h4>-->
        <a class="twitter-timeline" data-partner="tweetdeck" href="https://twitter.com/robvanderg/timelines/1040524713066414081?ref_src=twsrc%5Etfw">Example Tweets - Curated tweets by robvanderg</a> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

            <!--<a class="twitter-timeline"  data-show-replies="false" data-chrome="noheader"  href="https://twitter.com/search?q=2mr%20OR%202mrw%20OR%202day%20OR%204eva%20OR%20cuz%20OR%20nite" data-widget-id="927890869226758145">Tweets about 2mr OR 2mrw OR 2day OR 4eva OR cuz OR nite</a>-->
            <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
  </div>
  </body>
</html>

