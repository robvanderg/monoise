cd /home/rob/projects/monoise/src/
if ps aux | grep "/demo/bin/monoise" | grep -v grep > /dev/null
then
    echo "Running" >> ../status
else
    echo "Stopped" >> ../status
    date >> ../status
    ./demo/bin/monoise -m DE >> ../stdout 2>> ../stderr &
fi

