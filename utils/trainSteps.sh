echo "You need to have a file called tweets.txt"

# get language predictions
cd fastText
make
./fasttext predict-prob lid.176.bin ../tweets.txt 2  | sed  's;__label__;;g' > ../tweets.fast

# select tweets
cd ..
paste tweets.fast tweets.txt | grep -a "^en"  | cut -f 2-9999 -d '	' | sed "s/&lt;/</g" | sed "s/&gt;/>/g" | sed "s/&amp;/&/g" | sed "s/&quot;/\"/g" | sed "s/&#39;/'/g" > tweets.en

# train embeds
cd word2vec
make
./word2vec -train ../tweets.en  -threads 12 -cbow 0 -binary 1 -output ../../embeds.en

# cache embeds
cd cacheembeds/cachePython/
python3 cache.py --input ../../tweets.en ../../embeds.en ../../embeds.en.cache
cd src
icmbuild
./tmp/bin/binary ../../embeds.en ../../embeds.en.cache ../../embeds.en.cache

# train n-grams twitter
cd ../../../utils/ngrams/
icmbuild
./tmp/bin/binary ../../tweets.en ../../tw.ngr.en 3

# train n-grams wikipedia
./wiki.sh en
zcat wiki.*en.gz > wiki.en
./tmp/bin/binary wiki.en ../../wiki.ngr.en 3

# get aspell model
cd data/tr
wget http://tug.ctan.org/support/aspell/dict/tr/aspell-tr-0.50-0.tar.bz2
tar xjf aspell-tr-0.50-0.tar.bz2
cd aspell-tr-0.50-0/
./configure
make
cp ../../en/aspell-model/iso-8859-* .
cp ../../en/aspell-model/standard.kbd .
cd ..
aspell --dict-dir=./aspell-tr-0.50-0/ --lang=tr dump master | aspell -l tr expand --dict-dir=./aspell-tr-0.50-0/ > aspell
mv aspell-tr-0.50-0 data/tr/aspell-model



