import sys

if len(sys.argv) < 4:
    print('please give norm file and begin and end of new file')
    exit(1)


beg = int(sys.argv[2])
end = int(sys.argv[3])

sentCounter = 0
first = True
for line in open(sys.argv[1]):
    if len(line.split()) < 2 or first:
        #print(line.strip(), end='')
        sentCounter += 1
        first = False
    if sentCounter > beg and sentCounter <= end:
        print(line, end='')
    if sentCounter > end:
        break

