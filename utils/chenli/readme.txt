This dataset was copied with permission of Chen Li after his version was not available online anymore; see below the original README

============================================================================
    Text Normalization Data Set v1.0

    Copyright (C) 2014
    Computer Science Department, The University of Texas at Dallas
=============================================================================

1. Copyright Notice

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.


2. Data Format
   
    The text normalization data set contains 2577 tweets with
	non-standard tokens and their human-annotated normalized word forms. 
    These tweets were manually normalized by the Amazon Mechanical Turk annotators at the sentence level. 
    Note that the annotation results may be subject to the turkers' understanding of the social media language. 
  
    Each line of the data file is in the following format:
    
    non-standard token \t NEED_NORM \t standard word
	OR
	standard word \t NORMED standard word

	NEED_NORM means the token in the left need normalization and its possible correct form is in the right
	NORMED means this token has been a correct word.
	
    Then every tweet is separated by a empty line.

3. If you find this data set helpful, please cite the following papers:

    (1)

    Chen Li and Yang Liu
    Improving Text Normalization using Character-Blocks Based Models and System Combination.
    In the Proceeding of COLING, 2012
    Page 1587-1602

    bib:
	@InProceedings{li-liu:2012:PAPERS,
		author    = {Li, Chen  and  Liu, Yang},
		title     = {Improving Text Normalization using Character-Blocks Based Models and System Combination},
		booktitle = {Proceedings of COLING 2012},
		month     = {December},
		year      = {2012},
		address   = {Mumbai, India},
		publisher = {The COLING 2012 Organizing Committee},
		pages     = {1587--1602},
		url       = {http://www.aclweb.org/anthology/C12-1097}
	}

    (2) 
    
    Chen Li and Yang Liu
    Improving Text Normalization via Unsupervised Model and Discriminative Reranking
    In the Processing of ACL SRW, 2014
    Page 86-93

    bib:
	@InProceedings{li-liu:2014:P14-3,
		author    = {Li, Chen  and  Liu, Yang},
		title     = {Improving Text Normalization via Unsupervised Model and Discriminative Reranking},
		booktitle = {Proceedings of the ACL 2014 Student Research Workshop},
		month     = {June},
		year      = {2014},
		address   = {Baltimore, Maryland, USA},
		publisher = {Association for Computational Linguistics},
		pages     = {86--93},
		url       = {http://www.aclweb.org/anthology/P/P14/P14-3012}
	}


For any questions and comments, please email chenli@hlt.utdallas.edu


