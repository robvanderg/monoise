import sys

for line in open(sys.argv[1]):
    tok = line.strip().split('\t')
    if len(line.strip()) == 0:
        print()
    elif len(tok) == 2:
        print('\t'.join([tok[0], 'word', tok[1]]))
    else:
        #print('ERROR', line)
        pass

