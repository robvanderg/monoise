#!/bin/bash

# Script to download wikipedia dumps and clean them
# Usage:
# ./wiki.sh <lang_code>
#
# you can replace the string `latest' with a date to get a specific dump
# note that this does not save the date of the dump, but rather the date it is collected

DATE='latest'

if [ "$#" -ne 1 ]; then
    echo "Please specify languagecode as first argument, e.g."
    echo "./wiki.sh fy"
    exit
fi

wget https://dumps.wikimedia.org/"$1"wiki/"$DATE"/"$1"wiki-"$DATE"-pages-articles-multistream.xml.bz2
if [ ! -f "$1"wiki-"$DATE"-pages-articles-multistream.xml.bz2 ]; then
    echo https://dumps.wikimedia.org/"$1"wiki/"$DATE"/"$1"wiki-"$DATE"-pages-articles-multistream.xml.bz2 could not be found
    echo Probably because "$1" is not a valid language code for wikipedia, see https://dumps.wikimedia.org/
    exit
fi

if [ ! -d wikiextractor ]; then
    git clone https://github.com/attardi/wikiextractor.git
fi

cd wikiextractor
python3 WikiExtractor.py  -o extracted."$1" ../"$1"wiki-"$DATE"-pages-articles-multistream.xml.bz2

cat extracted."$1"/*/* | grep -v "^<" | sed -n '/^$/!{s/<[^>]*>//g;p;}' | gzip > ../wiki."$(date +"%Y%m%d")"."$1".gz

rm -r extracted."$1"
rm ../"$1"wiki-"$DATE"-pages-articles-multistream.xml.bz2

