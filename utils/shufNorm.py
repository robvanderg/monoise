import sys
import random

text = open(sys.argv[1]).readlines()

norm = []
sent = ''

for line in text:
    if len(line.split()) > 1:
        sent += line
    else:
        norm.append(sent)
        sent = ''

outFile = open(sys.argv[1] + '.shuf', 'w')
random.shuffle(norm)
for sent in norm:
    outFile.write(sent + '\n')

outFile.close()

