import sys

if len(sys.argv) < 3:
    print('please specifiy input and output file')
    exit(1)

def getMatches(word, tok):
    matches = []
    for i in range(len(tok)):
        if tok[i].find(word) >= 0:
            matches.append(i)
    return matches

def onlyAlpha(word):
    clean = ''
    for char in word:
        if char.isalpha():
            clean += char
    return clean

def insertNorm(norm, idx, tok, normTok, cats, idxs):
    split = tok[idx].find(norm[0])
    if split > 0:
        bef = tok[idx][:split]
        self = tok[idx][split:]
        tok = tok[:idx] + [bef] + [self] + tok[idx+1:]
        normTok = normTok[:idx] + [bef] + [self] + normTok[idx+1:]
        cats = cats[:idx] + ['-'] + cats[idx:]
        idx += 1
    split = tok[idx].find(norm[0])
    if split > 0:
        print("error: split > 0")
    if len(tok[idx]) > len(norm[0]):
        after = tok[idx][len(norm[0]):]
        self = tok[idx][:len(norm[0])]
        tok = tok[:idx] + [self] + [after] + tok[idx+1:]
        normTok = normTok[:idx] + [self] + [after] + normTok[idx+1:]
        cats = cats[:idx] + ['-'] + cats[idx:]
    if len(tok[idx]) == len(norm[0]):
        cats[idx] = norm[1]
        if norm[2] != '-':
            normTok[idx] = norm[2]
        idxs.append(idx)
    else:
        print('Error, still not equal length!?')
    return tok, normTok, cats, idxs

def printSent(origs, norms, cats, outFile):
    if len(origs) != len(norms) or len(origs) != len(cats):
        print("Error, lengths not equal: ", len(origs), len(norms), len(cats))
        return
    for orig, cat, norm in zip(origs, cats, norms):
        #if norm[0] != '@' and norm.find('http') < 0:
        #    norm = norm.replace('_', ' ')
        outFile.write('\t'.join([orig, cat, norm]) + '\n')
    outFile.write('\n')

def process(tweet, norms, outFile):
    tok = tweet.split()
    normTok = tweet.split()
    cats = ['-'] * len(normTok)
    idxs = []
    for norm in norms:
        matches = getMatches(norm[0], tok)
        if len(matches) == 0:
            print('ERROR: ' + norm[0] + ' not found in tweet: ' + tweet)#??
        else:
            equals = []
            if len(matches) == 1:
                tok, normTok, cats, idxs = insertNorm(norm, matches[0], tok, normTok, cats, idxs)
            else:
                for match in matches:
                    cleaned = onlyAlpha(tok[match])
                    if (norm[0] == cleaned or norm[0] == tok[match]) and (len(idxs) == 0 or match > idxs[-1]):
                        equals.append(match)
                if len(equals) == 1 and (len(idxs) == 0 or equals[0] > idxs[-1]):
                    tok, normTok, cats, idxs = insertNorm(norm, equals[0], tok, normTok, cats, idxs)
                else:
                    added = False
                    for equal in equals:
                        if len(idxs) == 0 or equal > idxs[-1]:
                            tok, normTok, cats, idxs = insertNorm(norm, equal, tok, normTok, cats, idxs)
                            added = True
                            break
                    if not added:
                        for match in matches:
                            if len(idxs) == 0 or match > idxs[-1]:
                                tok, normTok, cats, idxs = insertNorm(norm, match, tok, normTok, cats, idxs)
                                break
    printSent(tok, normTok, cats, outFile)

outFile = open(sys.argv[2], 'w')

norms = []
tweet = ''
for line in open(sys.argv[1]):
    if line[:5].isdigit():
        if tweet != '':
            process(tweet, norms, outFile)
        tweet = line.strip().split('\t')[1]
        norms = []
    elif line[0] == '\t':
        norms.append(line[1:].strip().split(' '))
    elif line.startswith('    '):
        norms.append(line.strip().split(' '))
    else:
        print('weird line: ' + line)
        norms.append(line.strip().split(' '))

process(tweet, norms, outFile)
outFile.close()

